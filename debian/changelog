pt-websocket (0.2-9) unstable; urgency=medium

  * Export a writable home directory. (Closes: #952330)
  * Respect nocheck in DEB_BUILD_OPTIONS.
  * Update Vcs-Git and Vcs-Browser.
  * Specify Rules-Requires-Root: no.
  * Drop unused "lack of hardening" Lintian overrides.
  * Drop some boilerplate in debian/rules.
  * Bump Standards-Version to 4.5.0.

 -- Chris Lamb <lamby@debian.org>  Sun, 23 Feb 2020 16:34:24 -0800

pt-websocket (0.2-8) unstable; urgency=medium

  * Team upload.
  * Set Built-Using

 -- Michael Stapelberg <stapelberg@debian.org>  Mon, 05 Mar 2018 18:33:57 +0100

pt-websocket (0.2-7) unstable; urgency=medium

  * Team upload.
  * Set XS-Go-Import-Path

 -- Michael Stapelberg <stapelberg@debian.org>  Fri, 09 Feb 2018 20:07:52 +0100

pt-websocket (0.2-6) unstable; urgency=medium

  * Team upload.

  * Declare compliance with policy version 4.1.3
    * Update debian/copyright to refer to the text of the CC0 license in
      /usr/share/common-licenses

 -- Nicolas Braud-Santoni <nicolas@braud-santoni.eu>  Sun, 14 Jan 2018 15:34:00 +0100

pt-websocket (0.2-5) unstable; urgency=medium

  * Team upload.

  * debian/watch: Fix URL
  * Lintian
    * Remove unused override
    * Override hardening-related warnings
      These do not make sense for Go binaries.
  * Switch to debhelper 10
  * Declare compliance with policy version 4.1.2
    * debian/copyright: Use HTTPS URI for the file format
    * Compliance with 4.1.3 pending on base-files shipping the CC0 license

 -- Nicolas Braud-Santoni <nicolas@braud-santoni.eu>  Thu, 11 Jan 2018 12:54:16 +0100

pt-websocket (0.2-4) unstable; urgency=medium

  * Team upload.

  [ Ximin Luo ]
  * Update debian/watch

  [ Petter Reinholdtsen ]
  * Added homepage url to d/control file.

 -- Petter Reinholdtsen <pere@debian.org>  Tue, 09 Aug 2016 16:11:15 +0200

pt-websocket (0.2-3) unstable; urgency=medium

  * Use updated build dependency.

 -- Ximin Luo <infinity0@debian.org>  Wed, 18 May 2016 20:22:54 +0200

pt-websocket (0.2-2) unstable; urgency=medium

  * Change import to new golang.org/x/net/websocket. (Closes: #822385)
  * Update to latest Standards-Version. No changes required.

 -- Ximin Luo <infinity0@debian.org>  Thu, 12 May 2016 19:55:57 +0200

pt-websocket (0.2-1) unstable; urgency=low

  * Initial release (Closes: #739626)

 -- Ximin Luo <infinity0@pwned.gg>  Sun, 01 Jun 2014 13:40:19 +0200
